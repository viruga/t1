<?php
include_once "../mod/validate.php";
include_once "../crud/add.php";
include_once "../crud/find_user.php";

$message = '';
const MIN_LEN_LOGIN = 6;
const MIN_LEN_PASSWORD = 6;
const MIN_LEN_USERNAME = 2;
$error_fields = [];


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $login = trim($_POST['login']);

    if (validateEmpty($login) == false) {
        $error_fields ['login'] = "Поле не заполнено";
    } else if (validateMinLen($login,MIN_LEN_LOGIN) === false) {
        $error_fields ['login'] = "Минимальная длина поля ".MIN_LEN_LOGIN;
    } else if (doubleName($login, 'login') === false) {
        $error_fields ['login'] = "Пользователь с таким Login уже зарегистрирован";
    }

    $password = trim($_POST['password']);
    $confirm_password = trim($_POST['confirm_password']);

    if (validateEmpty($password) == false) {
        $error_fields ['password'] = "Поле не заполнено";
    } else if (validateMinLen($password,MIN_LEN_PASSWORD) === false) {
        $error_fields ['password'] = "Минимальная длина поля ".MIN_LEN_PASSWORD;
    } else if (validateAlnum($password) === false) {
        $error_fields ['password'] = "Поле может состоять только из Цифр и Англ.Букв";
    } else if ($password != $confirm_password) {
        $error_fields ['password'] = "Пароли не совпадают";
    }

    $email = trim($_POST['email']);

    if (validateEmpty($email) == false) {
        $error_fields ['email'] = "Поле не заполнено";
    } else if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $error_fields ['email'] = "Проверьте правильность заполнения поля";
    } else if (doubleName($email, 'email') === false) {
        $error_fields ['email'] = "Пользователь с таким email уже зарегистрирован";
    }

    $username = trim($_POST['username']);

    if (validateEmpty($username) === false) {
        $error_fields ['username'] = "Поле не заполнено";
    } else if (validateMinLen($username,MIN_LEN_USERNAME) === false) {
        $error_fields ['username'] = "Минимальная длина поля ".MIN_LEN_USERNAME;
    } else if (validateAlpha($username) === false) {
        $error_fields ['username'] = "Поле может содерать только буквы";
    }


    if (!empty($error_fields)) {
        $response = ["status" => false];
        $response = array_merge($response, $error_fields);
    } else {
        $response = [
            "status" => true,
        ];
    }

    if (empty($error_fields)) {
        $sault = RandomString();
        $password = sha1($password . $sault);

        $user = new User ($login, $password, $email, $username, $sault);
        $user->addUser();
        $response = [
            "status" => true,
        ];
    }


    echo json_encode($response);

}


?>