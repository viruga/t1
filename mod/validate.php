<?php
function validateEmpty($fieldName) {
    if (isset($fieldName) && !empty(trim($fieldName))) {
        return $fieldName;
    } else {
        return false;
    }
}

function validateMinLen($fieldName,$length) {
    if (strlen(trim($fieldName))>=$length) {
        return $fieldName;
    } else {
        return false;
    }
}

function validateAlnum ($fieldName) {
    if (ctype_alnum($fieldName)!==false) {
        return $fieldName;
    } else {
        return false;
    }
}

function validateAlpha ($fieldName) {
    if(preg_match('/^[а-яА-Яa-zA-ZёЁ]+$/u', $fieldName)){
        return $fieldName;
    } else {
        return false;
    }
}

function RandomString()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randstring = $characters[rand(0, strlen($characters))];
    return $randstring;
}

function doubleName ($name,$conn)
{

    $find = new FindUser;
    if ($find->findLogin($name,$conn) == false) {
        return false;
    } else {
        return true;
    }


}
function check_auth(): bool
{
    return !!($_SESSION['user_id'] ?? false);
}

?>
