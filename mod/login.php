<?php
session_start();
include_once "../mod/validate.php";
include_once "../crud/find_user.php";

$error_fields = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//Учитывать пробелы в начале и в конце как ошибка или же как совсем другой логин
    $login = trim($_POST['login']);
//Учитывать пробелы в начале и в конце как ошибка или же как совсем другой логин
    $login = strip_tags($login);
    $login = htmlspecialchars($login);

    $password = $_POST['password'];

    if ($login === '') {
        $error_fields[] = 'login';
    }

    if ($password === '') {
        $error_fields[] = 'password';
    }

    if (!empty($error_fields)) {
        $response = [
            "status" => false,
            "type" => 1,
            "message" => "Поле не заполнено",
            "fields" => $error_fields,
        ];

        echo json_encode($response);
        die();
    }

    $user = new FindUser();
    $user->Access($login,$password);
}


?>