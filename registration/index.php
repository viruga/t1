<noscript> <META HTTP-EQUIV="Refresh" CONTENT="0;URL=../nojs.php"> </noscript>
<?php
$titleName = "Регистрация пользователя.";
include_once "../template/header.php";
?>

        <div class="user__block">
            <div class="user__content">
                <h1 class="user__title">Регистрация</h1>
                <div class="text-field">
                    <form method="post" id="ajax_form" action="" >
                        <label class="text-field__label" for="login">Логин:</label>
                        <input class="text-field__input" type="text" name="login" id="login" placeholder="Login" >
                        <p class="login-error"></p>
                        <label class="text-field__label" for="password">Пароль:</label>
                        <input class="text-field__input" type="password" name="password" id="password" placeholder="Password">
                        <p class="password-error"></p>
                        <label class="text-field__label" for="confirm_password">Пароль:</label>
                        <input class="text-field__input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                        <label class="text-field__label" for="email">E-mail:</label>
                        <input class="text-field__input" type="text" name="email" id="email" placeholder="E-mail">
                        <p class="email-error"></p>
                        <label class="text-field__label" for="username">Имя:</label>
                        <input class="text-field__input" type="text" name="username" id="username" placeholder="Name">
                        <p class="username-error"></p>
                        <input type="submit" class="btn-register"  value="Регистрация">
                    </form>
                    <p class="error-txt"></p>
                </div>
            </div>
            <img src="/img/bg-about.svg" class="user__logo">
        </div>
<?php
include_once "../template/footer.php";
?>
