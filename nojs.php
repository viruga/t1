<?php
session_start();
$titleName = "Вход пользователя.";

include_once "template/header.php";
?>
            <div class="user__block">
            <div class="user__content">
                <h1 class="user__title">Ошибка</h1>
                <div class="text-field">
                    <p>Необходимо включить JS в браузере.
                    <p><a href="/login/" title="Вход на сайт">Вход</a> | <a href="/registration/" title="Регистрация пользователя">Регистрация</a>

                </div>
            </div>
        </div>
<?php
include_once "template/footer.php";
?>