<?php
class User {
    private string $login;
    private string $password;
    private string $email;
    private string $name;
    private string $sault;
    const DB_USERS = '../crud/db_users.json';
    public function __construct($login,$password,$email,$name,$sault)
    {
        $this->login=$login;
        $this->password=$password;
        $this->email=$email;
        $this->name=$name;
        $this->sault=$sault;
    }

    public function addUser()
    {
        $json = file_get_contents(self::DB_USERS);
        $json = json_decode($json, true);

        $user = [
                'login' => $this->login,
                'password' => $this->password,
                'email' => $this->email,
                'name' => $this->name,
                'sault' => $this->sault
        ];
        if ($json==null){
            $json = [];
        }
            array_push($json, $user);

        file_put_contents(self::DB_USERS, json_encode($json, JSON_FORCE_OBJECT | JSON_PRETTY_PRINT));

    }

}

?>