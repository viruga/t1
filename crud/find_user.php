<?php
class FindUser
{
    private string $login;

    private string $email;
    const DB_USERS = '../crud/db_users.json';

    public function findLogin($login,$name)
    {
        $users = file_get_contents(self::DB_USERS);
        $users = json_decode($users, true);

        foreach ($users as $user) {
            if (($user[$name]) == $login) {
                return false;
            }
        }

        return $login;
    }

    public function Access($login,$password)
    {
        $users = file_get_contents(self::DB_USERS);
        $users = json_decode($users, true);
        $response = [
            "status" => false,
            "message" => "Логин не найден",
        ];
        foreach ($users as $user) {
            if (($user['login']) == $login) {
                $noFind = '';
                if ($user['password'] == sha1($password . $user['sault'])) {
                    $_SESSION['login'] = $user['login'];
                    setcookie("user_name", $user['name'], time()+3600, '/');
                    $response = [
                        "status" => true,
                    ];
                    break;

                } else {
                      $response = [
                        "status" => false,
                        "message" => "Пароли не совпадают",
                    ];
                }
            }
        }

        echo json_encode($response);
    }
}

?>