$('.btn-login').click(function (e) {
    e.preventDefault();
    $(`input`).removeClass('error');

    let login = $('input[name="login"]').val(),
        password = $('input[name="password"]').val();
    $.ajax({
        url: '../mod/login.php',
        type: 'POST',
        dataType: 'json',
        data: {
            login: login,
            password: password,
        },
        success (data) {
            if (data.status) {
                document.location.href = '../login/';
            } else {
                if (data.type === 1) {
                    data.fields.forEach(function (field) {
                        $(`input[name="${field}"]`).addClass('error');
                    });
                }

                $('.error-txt').text(data.message);
            }

        }
    });

})


$('.btn-register').click(function (e) {
    e.preventDefault();
    $(`input`).removeClass('error');
    $('.login-error').text('');
    $('.password-error').text('');
    $('.email-error').text('');
    $('.username-error').text('');

    let login = $('input[name="login"]').val(),
        password = $('input[name="password"]').val(),
        confirm_password = $('input[name="confirm_password"]').val(),
        email = $('input[name="email"]').val(),
        username = $('input[name="username"]').val();


    $.ajax({
        url: '../mod/register.php',
        type: 'POST',
        dataType: 'json',
        data: {
            login: login,
            password: password,
            confirm_password: confirm_password,
            email: email,
            username: username
        },
         success (data) {
            if (data.status) {
                document.location.href = '../login/';
            } else {
                if (data.login) {
                   $('.login-error').text(data.login);
                   $('input[name="login"]').addClass('error');
                }
                if (data.password) {
                    $('.password-error').text(data.password);
                    $('input[name="password"]').addClass('error');
                    $('input[name="confirm_password"]').addClass('error');
                }
                if (data.email) {
                    $('.email-error').text(data.email);
                    $('input[name="email"]').addClass('error');
                }
                if (data.username) {
                    $('.username-error').text(data.username);
                    $('input[name="username"]').addClass('error');
                }


                $('.error-txt').text(data.message);
            }

        }



    });

})