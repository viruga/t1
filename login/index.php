<noscript> <META HTTP-EQUIV="Refresh" CONTENT="0;URL=../nojs.php"> </noscript>
<?php
session_start();
$titleName = "Вход пользователя.";

include_once "../template/header.php";

    if (isset($_GET['logout'])) {
        if (!empty($_SESSION['login']) && (!empty($_COOKIE['user_name']))) {
            setcookie("user_name", "", time()-3600, '/');
            session_destroy();
            header("Location:".$_SERVER['HTTP_REFERER']);
            die();
        }
    }


?>

<?php if (!empty($_SESSION['login']) && (!empty($_COOKIE['user_name']))) {
    echo "Добро пожаловать: ".htmlspecialchars($_COOKIE['user_name'])." <a href=\"?logout\">Выйти</a>";
} else {?>
        <div class="user__block">
            <div class="user__content">
                <h1 class="user__title">Вход пользователя</h1>
                <div class="text-field">
                    <form method="post" id="ajax_form" action="" >
                        <label class="text-field__label" for="echo json_encode($result); ">Логин:</label>
                        <input class="text-field__input" type="text" name="login" id="login" placeholder="Login">
                        <label class="text-field__label" for="password">Пароль:</label>
                        <input class="text-field__input" type="password" name="password" id="password" placeholder="Password">
                        <input type="submit" class="btn-login" value="Войти">
                    </form>
                    <p class="error-txt"></p>
                </div>
            </div>
            <img src="/img/bg-about.svg" class="user__logo">
        </div>
<?php } ?>
<?php
include_once "../template/footer.php";
?>
